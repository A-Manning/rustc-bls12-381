use ff::Field;
use pairing::bls12_381;
use pairing::bls12_381::Fr;

use super::reader::read_fr;
use super::writer::write_fr;
use libc::c_uchar;

use super::LENGTH_FR_BYTES;

// Check that a point is in the field.
#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_is_in_field(
    element: *const [c_uchar; LENGTH_FR_BYTES]
) -> bool {
    read_fr(unsafe { &*element }).is_ok()
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_is_zero(element: *const [c_uchar; LENGTH_FR_BYTES]) -> bool {
    let fr_elem = read_fr({ unsafe { &*element } });
    match fr_elem {
        Err(_) => false,
        Ok(fr_elem) => fr_elem.is_zero(),
    }
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_is_one(element: *const [c_uchar; LENGTH_FR_BYTES]) -> bool {
    let fr_elem = read_fr({ unsafe { &*element } });
    match fr_elem {
        Err(_) => false,
        Ok(fr_elem) => fr_elem == bls12_381::Fr::one(),
    }
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_zero(buffer: *mut [c_uchar; LENGTH_FR_BYTES]) {
    let zero = bls12_381::Fr::zero();
    write_fr(buffer, zero);
    // FIXME: Not sure the next line does work and/or may introduce bugs
    // assert!(rustc_bls12_381_fr_is_zero(buffer), true);
    ()
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_one(buffer: *mut [c_uchar; LENGTH_FR_BYTES]) {
    let one = bls12_381::Fr::one();
    write_fr(buffer, one);
    // FIXME: Not sure the next line does work and/or may introduce bugs
    // assert!(rustc_bls12_381_fr_is_one(buffer), true);
    ()
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_random(buffer: *mut [c_uchar; LENGTH_FR_BYTES]) {
    let mut random_gen = rand::thread_rng();
    let random_x = Fr::random(&mut random_gen);
    write_fr(buffer, random_x);
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_add(
    buffer: *mut [c_uchar; LENGTH_FR_BYTES],
    x: *const [c_uchar; LENGTH_FR_BYTES],
    y: *const [c_uchar; LENGTH_FR_BYTES],
) {
    let x = read_fr({ unsafe { &*x } }).expect("The first component is a not a Fr element");
    let y = read_fr({ unsafe { &*y } }).expect("The second component is not a Fr element");
    let mut sum = bls12_381::Fr::zero();
    sum.add_assign(&x);
    sum.add_assign(&y);
    write_fr(buffer, sum);
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_mul(
    buffer: *mut [c_uchar; LENGTH_FR_BYTES],
    x: *const [c_uchar; LENGTH_FR_BYTES],
    y: *const [c_uchar; LENGTH_FR_BYTES],
) {
    let x = read_fr({ unsafe { &*x } }).expect("The first component is a not a Fr element");
    let y = read_fr({ unsafe { &*y } }).expect("The second component is not a Fr element");
    let mut prod = bls12_381::Fr::one();
    prod.mul_assign(&x);
    prod.mul_assign(&y);
    write_fr(buffer, prod);
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_eq(
    x: *const [c_uchar; LENGTH_FR_BYTES],
    y: *const [c_uchar; LENGTH_FR_BYTES],
) -> bool {
    // FIXME/IMPROVEME: Is it the correct way of checking the equality? Depends
    // on the library implementation of ==.
    let x = read_fr({ unsafe { &*x } }).expect("The first component is a not a Fr element");
    let y = read_fr({ unsafe { &*y } }).expect("The second component is not a Fr element");
    x == y
}

/* BE CAREFULE: DO NOT CHECK IF INVERSIBLE!!! */
#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_unsafe_inverse(
    buffer: *mut [c_uchar; LENGTH_FR_BYTES],
    x: *const [c_uchar; LENGTH_FR_BYTES],
) {
    let x = read_fr({ unsafe { &*x } }).expect("The first component is a not a Fr element");
    let mut inverse = bls12_381::Fr::zero();
    inverse.add_assign(&x);
    inverse.inverse().unwrap();
    write_fr(buffer, inverse)
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_negate(
    buffer: *mut [c_uchar; LENGTH_FR_BYTES],
    x: *const [c_uchar; LENGTH_FR_BYTES],
) {
    let x = read_fr({ unsafe { &*x } }).expect("The first component is a not a Fr element");
    let mut opposite = bls12_381::Fr::zero();
    opposite.add_assign(&x);
    opposite.negate();
    write_fr(buffer, opposite)
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_square(
    buffer: *mut [c_uchar; LENGTH_FR_BYTES],
    x: *const [c_uchar; LENGTH_FR_BYTES],
) {
    let x = read_fr({ unsafe { &*x } }).expect("The first component is a not a Fr element");
    let mut result = bls12_381::Fr::zero();
    result.add_assign(&x);
    result.square();
    write_fr(buffer, result)
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fr_double(
    buffer: *mut [c_uchar; LENGTH_FR_BYTES],
    x: *const [c_uchar; LENGTH_FR_BYTES],
) {
    let x = read_fr({ unsafe { &*x } }).expect("The first component is a not a Fr element");
    let mut result = bls12_381::Fr::zero();
    result.add_assign(&x);
    result.double();
    write_fr(buffer, result)
}

#[cfg(test)]
mod tests {
    use super::*;
    // FIXME: It does not work!!! Please write tests. At the moment, the
    // correctness is verified using the OCaml binding.
    #[test]
    fn test_write_followed_by_read() {
        let zero = bls12_381::Fr::zero();
        let mut buffer: [c_uchar; LENGTH_FR_BYTES] = [0; LENGTH_FR_BYTES];
        let read_ptr_buffer: *const [c_uchar; LENGTH_FR_BYTES] = &buffer;
        let write_ptr_buffer: *mut [c_uchar; LENGTH_FR_BYTES] = &mut buffer;

        write_fr(write_ptr_buffer, zero);
        let read_value = read_fr({ unsafe { &*read_ptr_buffer } });
        assert!(read_value.unwrap().is_zero(), true)
    }

    #[test]
    fn test_rustc_bls12_381_fr_is_zero() {
        let zero = bls12_381::Fr::zero();
        let mut buffer: [c_uchar; LENGTH_FR_BYTES] = [0; LENGTH_FR_BYTES];
        let write_ptr_buffer: *mut [c_uchar; LENGTH_FR_BYTES] = &mut buffer;
        let read_ptr_buffer: *const [c_uchar; LENGTH_FR_BYTES] = &buffer;
        write_fr(write_ptr_buffer, zero);
        assert!(rustc_bls12_381_fr_is_zero(read_ptr_buffer), true)
    }
}
