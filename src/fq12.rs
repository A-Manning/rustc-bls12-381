use ff::Field;
use pairing::bls12_381;
use pairing::bls12_381::Fq12;

use super::reader::read_fq12;
use super::writer::write_fq12;

use super::LENGTH_FQ12_BYTES;
use libc::c_uchar;

// Check that a point is in the field.
#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_is_in_field(
    element: *const [c_uchar; LENGTH_FQ12_BYTES]
) -> bool {
    read_fq12(unsafe { &*element }).is_ok()
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_is_zero(g: *const [c_uchar; LENGTH_FQ12_BYTES]) -> bool {
    let g = read_fq12({ unsafe { &*g } }).expect("The parameter is a not a Fq12 element");
    g.is_zero()
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_is_one(g: *const [c_uchar; LENGTH_FQ12_BYTES]) -> bool {
    let g = read_fq12({ unsafe { &*g } }).expect("The parameter a not a Fq12 element");
    g == Fq12::one()
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_random(buffer: *mut [c_uchar; LENGTH_FQ12_BYTES]) {
    let mut random_gen = rand::thread_rng();
    let random = bls12_381::Fq12::random(&mut random_gen);
    write_fq12(buffer, random);
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_one(buffer: *mut [c_uchar; LENGTH_FQ12_BYTES]) {
    let one = bls12_381::Fq12::one();
    write_fq12(buffer, one);
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_zero(buffer: *mut [c_uchar; LENGTH_FQ12_BYTES]) {
    let zero = bls12_381::Fq12::zero();
    write_fq12(buffer, zero);
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_add(
    buffer: *mut [c_uchar; LENGTH_FQ12_BYTES],
    g1: *const [c_uchar; LENGTH_FQ12_BYTES],
    g2: *const [c_uchar; LENGTH_FQ12_BYTES],
) {
    let g1 = read_fq12({ unsafe { &*g1 } }).expect("The first component is a not a Fq12 element");
    let g2 = read_fq12({ unsafe { &*g2 } }).expect("The second component is not a Fq12 element");
    let mut sum = bls12_381::Fq12::zero();
    sum.add_assign(&g1);
    sum.add_assign(&g2);
    write_fq12(buffer, sum);
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_mul(
    buffer: *mut [c_uchar; LENGTH_FQ12_BYTES],
    g1: *const [c_uchar; LENGTH_FQ12_BYTES],
    g2: *const [c_uchar; LENGTH_FQ12_BYTES],
) {
    let g1 = read_fq12({ unsafe { &*g1 } }).expect("The first component is a not a Fq12 element");
    let g2 = read_fq12({ unsafe { &*g2 } }).expect("The second component is not a Fq12 element");
    let mut prod = bls12_381::Fq12::one();
    prod.mul_assign(&g1);
    prod.mul_assign(&g2);
    write_fq12(buffer, prod);
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_eq(
    g1: *const [c_uchar; LENGTH_FQ12_BYTES],
    g2: *const [c_uchar; LENGTH_FQ12_BYTES],
) -> bool {
    let g1 = read_fq12({ unsafe { &*g1 } }).expect("The first component is a not a Fq12 element");
    let g2 = read_fq12({ unsafe { &*g2 } }).expect("The second component is not a Fq12 element");
    g1 == g2
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_unsafe_inverse(
    buffer: *mut [c_uchar; LENGTH_FQ12_BYTES],
    g: *const [c_uchar; LENGTH_FQ12_BYTES],
) {
    let g = read_fq12({ unsafe { &*g } }).expect("The parameter is not a Fq12 element");
    let mut inverse = bls12_381::Fq12::zero();
    inverse.add_assign(&g);
    inverse.inverse();
    write_fq12(buffer, inverse);
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_negate(
    buffer: *mut [c_uchar; LENGTH_FQ12_BYTES],
    g: *const [c_uchar; LENGTH_FQ12_BYTES],
) {
    let g = read_fq12({ unsafe { &*g } }).expect("The parameter is not a Fq12 element");
    let mut opposite = bls12_381::Fq12::zero();
    opposite.add_assign(&g);
    opposite.negate();
    write_fq12(buffer, opposite);
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_square(
    buffer: *mut [c_uchar; LENGTH_FQ12_BYTES],
    x: *const [c_uchar; LENGTH_FQ12_BYTES],
) {
    let x = read_fq12({ unsafe { &*x } }).expect("The first component is a not a Fq12 element");
    let mut result = bls12_381::Fq12::zero();
    result.add_assign(&x);
    result.square();
    write_fq12(buffer, result)
}

#[no_mangle]
pub extern "C" fn rustc_bls12_381_fq12_double(
    buffer: *mut [c_uchar; LENGTH_FQ12_BYTES],
    x: *const [c_uchar; LENGTH_FQ12_BYTES],
) {
    let x = read_fq12({ unsafe { &*x } }).expect("The first component is a not a Fq12 element");
    let mut result = bls12_381::Fq12::zero();
    result.add_assign(&x);
    result.double();
    write_fq12(buffer, result);
}
